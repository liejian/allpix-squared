# Global configuration
[Allpix]
# Simulate a total of 5 events
number_of_events = 10000
# Use the short logging format
log_format = "SHORT"
# Location of the detector configuration
detectors_file = "jadepix_sensor.conf"

# Read and instantiate the detectors and construct the Geant4 geometry
[GeometryBuilderGeant4]

# Initialize physics list and particle source
[DepositionGeant4]
# Use a Geant4 physics lists with EMPhysicsStandard_option3 enabled
physics_list = FTFP_BERT_EMY
# Use a charged pion as particle
particle_type = "gamma"
# Set the energy of the particle
beam_energy = 5.9keV
beam_size = 500um
# Set the energy spread of the generated paricle beam
beam_energy_spread = 0.1keV
# Origin of the beam
beam_position = 0 0 -0.1mm
# The direction of the beam
beam_direction = 0 0 1
# Use a single particle in a single 'event'
number_of_particles = 1

enable_pai = true

[ElectricFieldReader]
name = "dut"
model = "init"
file_name = "../field/n2_dfise_ElectricField_user_origin.init"

# Propagate the charge carriers through the sensor
[GenericPropagation]
# Set the temperature of the sensor
temperature = 293K
# Propagate multiple charges at once
charge_per_step = 50

# Transfer the propagated charges to the pixels
[SimpleTransfer]
max_depth_distance = 5um

# Digitize the propagated charges
[DefaultDigitizer]
# Noise added by the readout electronics
electronics_noise = 30e
# Threshold for a hit to be detected
threshold = 10e
# Threshold dispersion
threshold_smearing = 10e
# Noise added by the digitisation
adc_smearing = 0e
adc_slope = 3.8e
adc_resolution = 16
gain = 8

# Save histograms to the ROOT output file
[DetectorHistogrammer]
# Save histograms for the "dut" detector only
name = "dut"

# Store all simulated objects to a ROOT file with TTrees
[ROOTObjectWriter]
# File name of the output file
file_name = "jadepix-user-origin"
# Ignore initially deposited charges and propagated carriers:
#exclude = DepositedCharge, PropagatedCharge

#[VisualizationGeant4]
##Use the Qt gui
#mode = "none"
## Set transparency of the detector models (in percent)
#transparency = 0.4
## Set viewing style (alternative is ’wireframe’)
#view_style = "surface"
## Color trajectories by charge of the particle
#trajectories_color_mode = "charge"
#trajectories_color_positive = "blue"
#trajectories_color_neutral = "green"
#trajectories_color_negative = "red"
