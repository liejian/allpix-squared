#include <fstream>
#include <string>
#include <vector> 
#include <iostream>
#include <memory>
#include <sstream>

void convert(std::string file_name, std::string outfile_name){
  std::ifstream file(file_name);

  if(file.fail()) {
    std::cerr << "invalid data or unexpected end of file" << std::endl;
    return;
  }

  std::ofstream outfile(outfile_name); 
  if(outfile.fail()) {
    std::cerr << "invalid file" << std::endl;
    return;
  }

  std::string header;
  std::getline(file, header);
  outfile << header << '\n';

  std::cout << "Header of file " << file_name << " is " << header << std::endl;

  std::string tmpline;
  std::getline(file, tmpline);
  int nlines=0;

  double thickness, xpixsz, ypixsz, temperature, flux, rhe, new_drde, tmp;
  size_t xsize, ysize, zsize;
  size_t xind, yind, zind;
  double xvalue, yvalue, zvalue;
  std::vector<size_t> xindVec, yindVec, zindVec;
  std::vector<double> xvalueVec, yvalueVec, zvalueVec;
  std::stringstream ss;
  
  while(std::getline(file, tmpline)){
    // print the init seed and cluster length
    // print the incident pion direction
    // print the magnetic field (specify separately)
    if(nlines<3){ 
      outfile << tmpline << '\n';
    }

    // Get information of electric field
    if(nlines == 3){
      ss.clear();
      ss.str(tmpline);
      ss >> thickness >> xpixsz >> ypixsz >> temperature >> flux >> rhe >> new_drde >> xsize >> ysize >> zsize >> tmp;
      outfile << ypixsz << " " << thickness << " " << xpixsz << " " << temperature << " " << flux << " " << rhe << " " << new_drde << " " << zsize << " " << xsize << " " << ysize << " " << tmp << '\n';
    }

    // Get index of electric field
    if(nlines > 3){
      ss.clear();
      ss.str(tmpline);
      ss >> xind >> yind >> zind >> xvalue >> yvalue >> zvalue;

      xindVec.push_back(xind);
      yindVec.push_back(yind);
      zindVec.push_back(zind);
      
      xvalueVec.push_back(xvalue);
      yvalueVec.push_back(yvalue);
      zvalueVec.push_back(zvalue);

    }
    nlines++;
  }
  for(size_t i=0; i<zsize; i++)
    for(size_t j=0; j<xsize; j++)
      for(size_t k=0; k<ysize; k++)
      {
        outfile << i+1 << " " 
                << j+1 << " "
                << k+1 << " "
                << zvalueVec.at(i*xsize*ysize+j*ysize+k) << " "
                << xvalueVec.at(i*xsize*ysize+j*ysize+k) << " "
                << yvalueVec.at(i*xsize*ysize+j*ysize+k) << "\n"; 
      }

  //for(size_t i=0; i<zsize; i++)
  //  for(size_t j=0; j<xsize; j++)
  //    for(size_t k=0; k<ysize; k++)
  //    {
  //      auto xrvalue = zvalueVec.at(i*xsize*ysize+j*ysize+k);
  //      auto yrvalue = xvalueVec.at(i*xsize*ysize+j*ysize+k);
  //      auto zrvalue = yvalueVec.at(i*xsize*ysize+j*ysize+k);
  //      
  //      if(i>14 && i<18 && k>14 && k<18 && j>15){
  //        xrvalue *= 1000;
  //        yrvalue *= 1000;
  //        zrvalue *= 1000;
  //      }
  //      
  //      outfile << i+1 << " " 
  //              << j+1 << " "
  //              << k+1 << " "
  //              << xrvalue << " "
  //              << yrvalue << " "
  //              << zrvalue << "\n"; 
  //    }

  file.close();
  outfile.close();
}


void print_usage(){
  std::cout << "NAME\n\tconvert - convert geometry\n" << std::endl;
  std::cout << "\nSYNOPSIS\n\tconvert input output\n " << std::endl; 
}

int main(int argc, char** argv) {
  if (argc < 2) {
    print_usage() ;  
    return -1; 
  }

  std::string inputFile = std::string(argv[1]);
  std::string outputFile = std::string(argv[2]);

  std::cout << "Input: " << inputFile << std::endl; 
  convert(inputFile, outputFile);

  std::cout << "File saved as: " << outputFile << std::endl;

}

