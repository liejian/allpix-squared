#include <fstream>
#include <string>
#include <vector> 
#include <iostream>
#include <memory>
#include <sstream>

void combine(std::string infile1_name, std::string infile2_name, std::string outfile_name){
  
  std::ifstream infile1(infile1_name);
  if(infile1.fail()) {
    std::cerr << "invalid data or unexpected end of file" << std::endl;
    return;
  }

  std::ifstream infile2(infile2_name); 
  if(infile2.fail()) {
    std::cerr << "invalid file" << std::endl;
    return;
  }

  std::ofstream outfile;
  outfile.open(outfile_name, std::ios::trunc); 
  if(outfile.fail()) {
    std::cerr << "invalid output file" << std::endl;
    return;
  }

  std::string header;
  std::getline(infile1, header);
  std::cout << "Header of file " << infile1_name << " is " << header << std::endl;
  outfile << header << '\n';
  
  std::getline(infile2, header);
  std::cout << "Header of file " << infile2_name << " is " << header << std::endl;

  std::string tmpline1, tmpline2;
  std::getline(infile1, tmpline1);
  outfile << tmpline1 << '\n';

  std::getline(infile2, tmpline2);

  int nlines1=0;
  int nlines2=0;

  double thickness1, xpixsz1, ypixsz1, temperature1, flux1, rhe1, new_drde1, tmp1;
  size_t xsize1, ysize1, zsize1;
  size_t xind1, yind1, zind1;
  double xvalue1, yvalue1, zvalue1;
  std::vector<size_t>  xind1Vec, yind1Vec, zind1Vec;
  std::vector<double> xvalue1Vec, yvalue1Vec, zvalue1Vec;
  
  std::stringstream ss;
  while(std::getline(infile1, tmpline1)){
    if(nlines1<2){ 
      outfile << tmpline1 << '\n';
    }

    // Get information of electric field
    if(nlines1 == 2){
      ss.clear();
      ss.str(tmpline1);
      std::cout << tmpline1 << std::endl; 
      ss >> thickness1 >> xpixsz1 >> ypixsz1 >> temperature1 >> flux1 >> rhe1 >> new_drde1 >> xsize1 >> ysize1 >> zsize1 >> tmp1;
    } 

    // Get index of electric field
    if(nlines1 > 2){
      ss.clear();
      ss.str(tmpline1);
      ss >> xind1 >> yind1 >> zind1 >> xvalue1 >> yvalue1 >> zvalue1;
      xind1Vec.push_back(xind1);
      yind1Vec.push_back(yind1);
      zind1Vec.push_back(zind1);
      
      xvalue1Vec.push_back(xvalue1);
      yvalue1Vec.push_back(yvalue1);
      zvalue1Vec.push_back(zvalue1);
    }
    nlines1++;
  }

  double thickness2, xpixsz2, ypixsz2, temperature2, flux2, rhe2, new_drde2, tmp2;
  size_t xsize2, ysize2, zsize2;
  size_t xind2, yind2, zind2;
  double xvalue2, yvalue2, zvalue2;
  std::vector<size_t>  xind2Vec, yind2Vec, zind2Vec;
  std::vector<double> xvalue2Vec, yvalue2Vec, zvalue2Vec;
  while(std::getline(infile2, tmpline2)){
    if(nlines2 == 2){
      ss.clear();
      ss.str(tmpline2);
      std::cout << tmpline2 << std::endl;
      ss >> thickness2 >> xpixsz2 >> ypixsz2 >> temperature2 >> flux2 >> rhe2 >> new_drde2 >> xsize2 >> ysize2 >> zsize2 >> tmp2;
    } 
    // Get index of electric field
    if(nlines2 > 2){
      ss.clear();
      ss.str(tmpline2);
      ss >> xind2 >> yind2 >> zind2 >> xvalue2 >> yvalue2 >> zvalue2;
      xind2Vec.push_back(xind2);
      yind2Vec.push_back(yind2);
      zind2Vec.push_back(zind2);

      xvalue2Vec.push_back(xvalue2);
      yvalue2Vec.push_back(yvalue2);
      zvalue2Vec.push_back(zvalue2);
    }
    nlines2++;
  }

  double thickness, xpixsz, ypixsz, temperature, flux, rhe, new_drde, tmp;
  size_t xsize, ysize, zsize;
  std::vector<size_t>  xindVec, yindVec, zindVec;
  std::vector<double> xvalueVec, yvalueVec, zvalueVec;
  
  thickness = thickness1 + thickness2;
  xpixsz = xpixsz1;
  ypixsz = ypixsz1;
  temperature = temperature1;
  flux = flux1;
  rhe = rhe1;
  new_drde = new_drde1;
  tmp = tmp1;
  xsize = xsize1;
  ysize = ysize1;
  zsize = zsize1 + zsize2;
  std::cout << thickness << " " << xpixsz << " " << ypixsz << " " << temperature << " " << flux << " "<< rhe << " " << new_drde << " " << xsize << " " << ysize << " " << zsize << " " << tmp << "\n";
  outfile << thickness << " " << xpixsz << " " << ypixsz << " " << temperature << " " << flux << " "<< rhe << " " << new_drde << " " << xsize << " " << ysize << " " << zsize << " " << tmp << "\n";

  for(size_t i=0; i<xsize; i++)
    for(size_t j=0; j<ysize; j++)
      for(size_t k=0; k<zsize; k++)
      {
        if(k<18){
          xindVec.push_back(xind1Vec.at(i*ysize1*zsize1+j*zsize1+k));
          yindVec.push_back(yind1Vec.at(i*ysize1*zsize1+j*zsize1+k));
          zindVec.push_back(zind1Vec.at(i*ysize1*zsize1+j*zsize1+k));

          xvalueVec.push_back(xvalue1Vec.at(i*ysize1*zsize1+j*zsize1+k));
          yvalueVec.push_back(yvalue1Vec.at(i*ysize1*zsize1+j*zsize1+k));
          zvalueVec.push_back(zvalue1Vec.at(i*ysize1*zsize1+j*zsize1+k));
        }
        else{
          xindVec.push_back(xind2Vec.at(i*ysize2*zsize2+j*zsize2+k-18));
          yindVec.push_back(yind2Vec.at(i*ysize2*zsize2+j*zsize2+k-18));
          zindVec.push_back(zind2Vec.at(i*ysize2*zsize2+j*zsize2+k-18)+zsize1);

          xvalueVec.push_back(xvalue2Vec.at(i*ysize2*zsize2+j*zsize2+k-18));
          yvalueVec.push_back(yvalue2Vec.at(i*ysize2*zsize2+j*zsize2+k-18));
          zvalueVec.push_back(zvalue2Vec.at(i*ysize2*zsize2+j*zsize2+k-18));
        }
      }

  for(size_t i=0; i<xsize; i++)
    for(size_t j=0; j<ysize; j++)
      for(size_t k=0; k<zsize; k++)
      {
        outfile << xindVec.at(i*ysize*zsize+j*zsize+k) << " " 
                << yindVec.at(i*ysize*zsize+j*zsize+k) << " "
                << zindVec.at(i*ysize*zsize+j*zsize+k) << " "
                << xvalueVec.at(i*ysize*zsize+j*zsize+k) << " "
                << yvalueVec.at(i*ysize*zsize+j*zsize+k) << " "
                << zvalueVec.at(i*ysize*zsize+j*zsize+k) << "\n"; 
      }

  infile1.close();
  infile2.close();
  outfile.close();
}

void print_usage(){
  std::cout << "NAME\n\tconvert - convert geometry\n" << std::endl;
  std::cout << "\nSYNOPSIS\n\tconvert input1 input2 output\n " << std::endl; 
}

int main(int argc, char** argv) {
  if (argc < 2) {
    print_usage() ;  
    return -1; 
  }

  std::string inputFile1 = std::string(argv[1]);
  std::string inputFile2 = std::string(argv[2]);
  std::string outputFile = std::string(argv[3]);

  std::cout << "Input1: " << inputFile1 << std::endl; 
  std::cout << "Input2: " << inputFile2 << std::endl; 
  std::cout << "Output: " << outputFile << std::endl; 

  combine(inputFile1, inputFile2, outputFile);
}

