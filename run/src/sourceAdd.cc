#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TH1.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

std::shared_ptr<TTree> sourceAddTree(TFile* file1, TFile* file2){

    TTree* cluster1_tree = static_cast<TTree*>(file1->Get("clusters"));
    if(!cluster1_tree) {
        std::cout << "Could not read tree cluster1, cannot continue." << std::endl;
        return std::make_shared<TTree>();
    }
    
    TTree* cluster2_tree = static_cast<TTree*>(file2->Get("clusters"));
    if(!cluster2_tree) {
        std::cout << "Could not read tree cluster2, cannot continue." << std::endl;
        return std::make_shared<TTree>();
    }


    std::vector<int>* input_cluster1_signal=0;
    std::vector<int>* input_cluster2_signal=0;
    
    cluster1_tree->SetBranchAddress("cluster_signal",&input_cluster1_signal);
    cluster2_tree->SetBranchAddress("cluster_signal",&input_cluster2_signal);
    
    auto output_tree = std::make_shared<TTree>("results", "information");
    
    std::vector<int> output_cluster_signal;
    
    output_tree->Branch("cluster_signal", &output_cluster_signal);

    for(Int_t i=0; i<cluster1_tree->GetEntries();++i){
      cluster1_tree->GetEntry(i); 
      output_cluster_signal.clear();
      for(auto& signal : (*input_cluster1_signal)){
        output_cluster_signal.push_back(signal);
      }
      output_tree->Fill();
    }

    for(Int_t i=0; i<cluster2_tree->GetEntries();++i){
      cluster2_tree->GetEntry(i); 
      output_cluster_signal.clear();
      for(auto& signal : (*input_cluster2_signal)){
        output_cluster_signal.push_back(signal);
      }
      output_tree->Fill();
    }
   
    return output_tree;
}

void print_usage(){
  printf("NAME\n\tsourceAdd - Gen Allpix root files\n");
  printf("\nSYNOPSIS\n\tsourceAdd input1 input2 output\n "); 

}

int main(int argc, char** argv) {
  if (argc < 2) {
    print_usage() ;  
    return -1; 
  }


  TString inputFile1 = std::string(argv[1]);
  TString inputFile2 = std::string(argv[2]);
  TString outputFile = std::string(argv[3]);


  printf("Input1: %s\n", inputFile1.Data()); 
  printf("Input2: %s\n", inputFile2.Data()); 


  auto fin1 = new TFile(inputFile1);
  auto fin2 = new TFile(inputFile2);

  auto fout = new TFile(outputFile, "RECREATE");

  auto tree = sourceAddTree(fin1, fin2);
  tree->Write();
  fout->Close(); 

  printf("File saved as: %s\n", outputFile.Data());

}

