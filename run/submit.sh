#!/usr/bin/env bash

# Main driver to submit 
# Author: 
#      Chen Liejian <chenlj@ihep.ac.cn> 
# Created [2018-03-02 Fri 19:24]


usage() {
    printf "NAME\n\tsubmit.sh - Main driver to submit\n"
    printf "\nSYNOPSIS\n"
    printf "\n\t%-5s\n" "./run.sh [OPTION]"
    printf "\nOPTIONS\n"
    printf "\n\t%-9s  %-40s"  "0.1"      "[Simulate jadepix]" 
    printf "\n\t%-9s  %-40s"  "0.1.1"    "Run jadepix1 conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.2"    "Run jadepix1 user conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.3"    "Run jadepix1 user origin conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.4"    "Run jadepix1 user test conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.5"    "Run jadepix1 user Sr-90 conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.6"    "Run jadepix1 user MEF conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.7"    "Run jadepix1 user MEF Sr-90 conf files" 
    printf "\n\t%-9s  %-40s"  "0.1.8"    "Run jadepix1 user telescope conf files" 
    printf "\n"  
    printf "\n\t%-9s  %-40s"  "0.2"      "Run genApx" 
    printf "\n\t%-9s  %-40s"  "0.2.1"    "Run genApx conf" 
    printf "\n\t%-9s  %-40s"  "0.2.2"    "Run genApx user" 
    printf "\n\t%-9s  %-40s"  "0.2.3"    "Run genApx user origin" 
    printf "\n"  
    printf "\n\t%-9s  %-40s"  "0.3"      "Run convert" 
    printf "\n\t%-9s  %-40s"  "0.3.1"    "Run convert electric field using AllPix Squared" 
    printf "\n\t%-9s  %-40s"  "0.3.2"    "Run convert electric field from TCAD, rotation" 
    printf "\n\t%-9s  %-40s"  "0.3.3"    "Run display electric field" 
    printf "\n\t%-9s  %-40s"  "0.3.4"    "Run convert 2D electric field using AllPix Squared" 
    printf "\n\t%-9s  %-40s"  "0.3.5"    "Run display Timepix electric field" 
    printf "\n\t%-9s  %-40s"  "0.3.6"    "Run convert electric field from TCAD using user tools[efconvert]" 
    printf "\n"  
    printf "\n\t%-9s  %-40s"  "0.4"      "Run Output EField" 
    printf "\n\t%-9s  %-40s"  "0.4.1"    "Run Output EField point" 
    printf "\n\t%-9s  %-40s"  "0.4.2"    "Run output conf efield" 
    printf "\n"  
    printf "\n\t%-9s  %-40s"  "0.5"      "Run ana" 
    printf "\n\t%-9s  %-40s"  "0.5.1"    "Run ana user" 
    printf "\n\t%-9s  %-40s"  "0.5.2"    "Run ana user test" 
    printf "\n\t%-9s  %-40s"  "0.5.3"    "Run ana Sr-90 test" 
    printf "\n\t%-9s  %-40s"  "0.5.4"    "Run ana MEF test" 
    printf "\n\t%-9s  %-40s"  "0.5.5"    "Run ana MEF Sr-90 test" 
}


if [[ $# -eq 0 ]]; then
    usage
    printf "\nPlease enter your option: "
    read option
else
    option=$1
fi

case $option in

    # --------------------------------------------------------------------------
    #  0.1 allpix-squared (v1.1.0)
    # --------------------------------------------------------------------------

    0.1) echo "Simulating jadepix1..."
         ;;
    0.1.1) echo "Running jadepix1 conf files..."
        allpix -c conf/jadepix_manual.conf -o output_directory="../output"
        ;;
    0.1.2) echo "Running jadepix1 user conf files..."
        allpix -c conf/jadepix_user.conf -o output_directory="../output"
        ;; 

    0.1.3) echo "Running jadepix1 user origin conf files..."
        allpix -c conf/jadepix_user_origin.conf -o output_directory="../output"
        ;; 

    0.1.4) echo "Running jadepix1 user test conf files..."
        allpix -c conf/jadepix_user_test.conf -o output_directory="../output"
        ;; 
    0.1.5) echo "Running jadepix1 user Sr-90 conf files..."
        allpix -c conf/jadepix_user_sr.conf -o output_directory="../output"
        ;; 
    0.1.6) echo "Running jadepix1 user MEF conf files..."
        allpix -c conf/jadepix_user_mef.conf -o output_directory="../output"
        ;; 
    0.1.7) echo "Running jadepix1 user MEF Sr-90 conf files..."
        allpix -c conf/jadepix_user_mef_sr.conf -o output_directory="../output"
        ;; 
    0.1.8) echo "Running jadepix1 user telescope conf files..."
        allpix -c conf/jadepix_user_telescope.conf -o output_directory="../output"
        ;; 

    # --------------------------------------------------------------------------
    #  0.2 genApx 
    # --------------------------------------------------------------------------

    0.2) echo "Running genApx..."
         ;;

    0.2.1) echo "Running genApx Conf..."
        ./bin/genApx output/jadepix-output.root output/jadepix_genapx.root 
        ;; 

    0.2.2) echo "Running genApx User..."
        ./bin/genApx output/jadepix-user.root output/jadepix_user_genapx.root 
        ;; 

    0.2.3) echo "Running genApx User Orign..."
        ./bin/genApx output/jadepix-user-origin.root output/jadepix_user_origin_genapx.root 
        ;; 

    # --------------------------------------------------------------------------
    #  0.3  convert
    # --------------------------------------------------------------------------

    0.3) echo "Running convert..."
         ;;

    0.3.1) echo "Run convert electric field using AllPix Squared" 
        cd field
        ../../bin/tcad_dfise_converter/dfise_converter -f n2_dfise -R 'Epi' -r 5 -x 33 -y 15 -z 33
        cd ..
        ;;

    0.3.2) echo "Running convert electric field from TCAD Rotation..."
        ./bin/convert field/n2_dfise_ElectricField.init field/n2_dfise_ElectricField_rotation.init
        ;; 

    0.3.3) echo "Running display electric field from TCAD..."
        cd field
        ../../bin/tcad_dfise_converter/mesh_plotter -f n2_dfise_ElectricField_rotation.init -x 33 -y 33 -z 15 -p xy -c 0
        cd ..
        ;; 

    0.3.4) echo "Run convert 2D electric field using AllPix Squared" 
        cd field
        ../../bin/tcad_dfise_converter/dfise_converter -f n226_dfise -R 'Epi' -d 2 -r 2 -x 1 -y 33 -z 18
        cd ..
        ;;

    0.3.5) echo "Running display Timepix electric field..."
        cd field
        ../../bin/tcad_dfise_converter/mesh_plotter -f jadepix1_electric_field.init -x 33 -y 33 -z 15 -p xy -c 15
        cd ..
        ;; 

    0.3.6) echo "Run convert electric field from TCAD using user tools[efconvert]" 
        cd field
        ../bin/efconvert -f n2_dfise -R "Epi+Footprint+nWell1+pWell1" -NX 33 -NY 18 -NZ 33 -r 5 -o field/n2_dfise_Epi_Well 
        cd ..
        ;; 
    # --------------------------------------------------------------------------
    #  0.4  EField
    # --------------------------------------------------------------------------

    0.4) echo "Running efield..."
         ;;

    0.4.1) echo "Run output user efield" 
        echo "Tips for displaying"
        echo "Using style [COLZ/ARR] for TH2D"
        echo "Using style [BOX2] for TH3D"
      ./bin/efield -f field/n34_dfise_combine.init -NX 33 -NY 33 -NZ 18 -cutX 16 -cutY 16 -cutZ 17 -d 18 -o output/n34_dfise_EFDisplay.root 
        ;;

    0.4.2) echo "Run output conf efield" 
        ./bin/efield -f field/jadepix1_electric_field.init -NX 25 -NY 17 -NZ 92 -cutX 10 -cutY 15 -cutZ 91 -o output/jadepix1_EFDisplay.root 
        ;;
    # --------------------------------------------------------------------------
    #  0.5 ana 
    # --------------------------------------------------------------------------

    0.5) echo "Running ana..."
         ;;

    0.5.1) echo "Running ana user..."
        ./bin/ana -f output/jadepix-user.root -s 1 -x 48 -y 16 -o output/jadepix-ana.root 
        ;; 

    0.5.2) echo "Running ana user test..."
        ./bin/ana -f output/jadepix-user-test.root -s 1 -x 48 -y 4 -o output/jadepix-ana-test.root 
        ;; 
    0.5.3) echo "Running ana user Sr-90..."
        ./bin/ana -f output/jadepix-user-sr.root -s 1 -x 48 -y 16 -o output/jadepix-ana-sr.root 
        ;; 
    0.5.4) echo "Running ana user MEF..."
        ./bin/ana -f output/jadepix-user-mef.root -s 1 -x 48 -y 16 -o output/jadepix-ana-mef.root 
        ;; 
    0.5.5) echo "Running ana user MEF Sr-90..."
        ./bin/ana -f output/jadepix-user-mef-sr.root -s 1 -x 48 -y 16 -o output/jadepix-ana-mef-sr.root 
        ;; 

esac
