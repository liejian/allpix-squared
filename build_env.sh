source /opt/allpix/build_env.sh
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/allpix/root/lib

export COMPILER_TYPE="llvm"

export PATH=$PATH:${HOME}/allpix-squared/run/bin

export PATH=$PATH:${HOME}/allpix-squared/bin
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/allpix-squared/lib
